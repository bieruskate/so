#! /bin/bash

directory=$1
extension=$2

test -e $directory/out && rm $directory/out

for file in $directory/*.$extension
do
	echo "$file" >> $directory/out
	cat $file >> $directory/out
done
