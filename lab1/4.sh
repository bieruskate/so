#! /bin/bash

directory=$1
counter=1

for file in `ls -S $directory`
do
	test -x $directory/$file && mv $directory/$file $directory/$file.$counter
	let counter+=1
done

