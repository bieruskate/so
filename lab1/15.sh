#! /bin/bash
#Michal Bieronski
#0F

directory=${1}
counter=0

for file in ${directory}/*
do
	test -x ${file} && let counter+=1
done

echo "Liczba plikow z prawem execute: ${counter}"
