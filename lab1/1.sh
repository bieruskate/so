#! /bin/bash

directory=$1

find $directory -name "*.old" -delete

for file in $directory/*
do
	test -w $file && mv $file $file".old"
done

