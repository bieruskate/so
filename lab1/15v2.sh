#! /bin/bash
#Michal Bieronski
#1F

directory=${1}
counter=0

if [ 1 -ne ${#} ];then
	echo "Niepoprawna liczba argumentow"
	exit 1

elif [ ! -w ${directory} ];then
	echo "Plik nie istnieje lub brak praw do zapisu"
	exit 2
fi

for file in ${directory}/*
do
	test -x ${file} && let counter+=1

	if [ -d ${file} ];then
		for subfile in ${file}/*;do
			test -x ${subfile} && let counter+=1
		done		
		for subfile in ${file}/*.no_count;do
			test -x ${subfile} && let counter-=1
		done		
	fi
done

for file in ${directory}/*.no_count
do
	test -x ${file} && let counter-=1
done

echo "Liczba plikow z prawem execute: ${counter}"

