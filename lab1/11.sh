#sprawdzenie czy użytkownik wpisał odpowiednie argumenty
if [ $# -lt 2 ]
then
        echo "Brak wymaganych argumentów"
        exit
fi

#sprawdzenie czy pierwszy argument jest katalogiem
if [ ! -d $1 ]
then
        echo "$1 nie jest katalogiem"
        exit
fi

#sprawdzenie czy drugi argument jest plikiem
if [ ! -f $2 ]
then
        echo "$2 nie jest plikiem lub podany plik nie istnieje"
        exit
fi

#sprawdzenie obecności plików z listy w folderze
for file_on_list in `cat $2`
do
        if [ ! -e $1/$file_on_list ]
        then
                missing_in_folder=$missing_in_folder" "$file_on_list
        fi
done


#sprawdzenie obecności plików z katalogu na liście
for file_in_folder in `ls $1`
do
        is_on=0
        for file_on_list in `cat $2`
        do
                if [ $file_in_folder = $file_on_list ]
                then
                        is_on=1
                        break
                fi
        done
        if [ $is_on -eq 0 ]
        then
                missing_on_list=$missing_on_list" "$file_in_folder
        fi
done


echo "Pliki nieobecne w folderze: "$missing_in_folder
echo "Pliki nieobecne na liście: "$missing_on_list
