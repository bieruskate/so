#! /bin/bash

directory=$1
list=$2

echo $directory

for name in `cat $list`
do
	new_file=$directory/$name
	echo $new_file

	if test -e $new_file
	then
		echo "Already exists - removing"
		rm $new_file
	else
		touch $new_file
	fi
done
