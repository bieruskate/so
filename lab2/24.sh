#! /bin/bash
#Michal Bieronski
#24

directory=$1

for file in $directory/*
do
	[ -L $file ] && echo "Symlinks in dir: "$file;
	[ -e $file ] && [ -L $file ] && echo "Path : "$(readlink $file);
done
