#! /bin/bash
#Michal Bieronski
#35

directory=${1}
target=${2}

command=${0}

if [ 2 -ne ${#} ];then
	echo "Wrong number of parameters"
	exit 1

elif [ ! -w ${directory} ] || [ ! -w ${target} ];then
	echo "File not exist or no write permission"
	exit 2
fi


for file in $directory/*
do
	if [ -e $file ] && [ -L $file ];then
		if [ "$(readlink $file)" = "$target" ];then
			echo $file

			if [[ "$target" = /* ]]; then
				echo "Absolute"
				real=$(realpath $file)
				ln -sfn $real $file
			fi
		fi
	fi

	if [ -e $file ] && [ ! -L $file ];then

		if [ $file -ef $target ]; then
			echo "Hard link: "$file
		fi
	fi

	if [ -d $file ];then
		$command $file $target
	fi

done
