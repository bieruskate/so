#! /bin/bash
#44
#Michal Bieronski

if [ ! -d $1 ]
then
	echo "File not exist or is not a directory" 
	exit 1
fi

find $1 -name ".git" -mtime -30 | xargs dirname
