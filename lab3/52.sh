#!/bin/bash
#52
#Michal Bieronski

wc_result=$(find $1 -executable -type f | wc)
wc_result=($wc_result)

echo "No of executables in directory: "${wc_result[1]}

executables=$(find $1 -executable -type f)

echo "Scripts:"
for file in $executables
do
	if [[ "$(file -i $file | cut -d' ' -f2)" == *"text/"* ]]
	then
		echo $file
		interpreter=$(head -n 1 $file | cut -d'!' -f2)
		[ -e $interpreter ] && echo "Interpreter exist"
	fi
done

echo "Modified in last week:" $(find $1 -mtime -7 -executable -type f)

