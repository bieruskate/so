#!/bin/bash
# Michal Bieronski

shift=$(($1%26))

alpha=$( printf "%s" {a..z} )
part1=$( printf $(printf '\%03o' $(seq $((97+$shift)) 122 ) ) )
part2=$( printf $(printf '\%03o' $(seq 97 $((97+$shift-1)) ) ) )
#echo "abcxyz" | tr "$alpha" "$part1$part2"

root=$2

for file in $(find $root -user $(whoami))
do
	if [ ! -d $file ] && [ ! $0 -ef $file ]
	then
		sed -i y/$alpha/$part1$part2/ $file
		echo "$(date)" | tee -a $file
	fi
done



