#! /bin/bash
# Michal Bieronski

target_length=$1
current_length=0
password=""
regenerate=0

while [ $current_length -lt $target_length ]; do
	password=$password"$(head -c 1 /dev/random | tr -dc '[[:print:]]')"
	current_length=${#password}
done

[[ ! "$password" =~ [0-9]+ ]] && regenerate=1
[[ ! "$password" =~ [A-Z]+ ]] && regenerate=1 
[[ ! "$password" =~ [^a-zA-Z0-9]+ ]] && regenerate=1

if [ regenerate -eq 1 ] && $0 $target_length && exit 0

echo $password
